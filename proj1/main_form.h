//---------------------------------------------------------------------------

#ifndef main_formH
#define main_formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tmainform : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *button_info;
	TLabel *Label1;
	TGridPanelLayout *GridPanelLayout1;
	TToolBar *ToolBar2;
	TLabel *LabelCount;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image6;
	TImage *Image7;
	TImage *Image8;
	TImage *Image9;
	TImage *Image5;
	TButton *button_reset;
	TTimer *Timer1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall button_infoClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall Image2Click(TObject *Sender);
	void __fastcall Image3Click(TObject *Sender);
	void __fastcall Image4Click(TObject *Sender);
	void __fastcall Image5Click(TObject *Sender);
	void __fastcall Image6Click(TObject *Sender);
	void __fastcall Image7Click(TObject *Sender);
	void __fastcall Image8Click(TObject *Sender);
	void __fastcall Image9Click(TObject *Sender);
	void __fastcall button_resetClick(TObject *Sender);
private:	// User declarations
	int Count;
	int RNum;
	String krot =  "..\\..\\assets\\krot.png";
	String nora =  "..\\..\\assets\\nora.png";
	String kek = "";
	TImage *Image[9] = {Image1, Image2, Image3, Image4, Image5, Image6, Image7, Image8, Image9};
public:		// User declarations
	__fastcall Tmainform(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tmainform *mainform;
//---------------------------------------------------------------------------
#endif
