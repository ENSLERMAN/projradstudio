object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 596
  Width = 817
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 720
    Top = 496
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    VendorLib = 'C:\code\projRadStudio\exam\assets\libpq.dll'
    Left = 600
    Top = 496
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=ensler'
      'User_Name=ensler'
      'Password=b4uld103'
      'Server=ensler.ru'
      'DriverID=PG')
    Connected = True
    LoginPrompt = False
    Left = 48
    Top = 504
  end
  object queryProducts: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select * from exam.product')
    Left = 80
    Top = 48
    object queryProductsid: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object queryProductscategory_id: TIntegerField
      FieldName = 'category_id'
      Origin = 'category_id'
    end
    object queryProductsname: TWideStringField
      FieldName = 'name'
      Origin = 'name'
      Size = 8190
    end
    object queryProductsgram: TIntegerField
      FieldName = 'gram'
      Origin = 'gram'
    end
    object queryProductskcal: TIntegerField
      FieldName = 'kcal'
      Origin = 'kcal'
    end
    object queryProductsprice: TFloatField
      FieldName = 'price'
      Origin = 'price'
    end
    object queryProductsnote: TWideStringField
      FieldName = 'note'
      Origin = 'note'
      Size = 8190
    end
    object queryProductssticker: TIntegerField
      FieldName = 'sticker'
      Origin = 'sticker'
    end
  end
  object GetByIdProduct: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select * from exam.product where exam.product.id =:ID;')
    Left = 176
    Top = 48
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object GetByIdProductid: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object GetByIdProductcategory_id: TIntegerField
      FieldName = 'category_id'
      Origin = 'category_id'
    end
    object GetByIdProductname: TWideStringField
      FieldName = 'name'
      Origin = 'name'
      Size = 8190
    end
    object GetByIdProductgram: TIntegerField
      FieldName = 'gram'
      Origin = 'gram'
    end
    object GetByIdProductkcal: TIntegerField
      FieldName = 'kcal'
      Origin = 'kcal'
    end
    object GetByIdProductprice: TFloatField
      FieldName = 'price'
      Origin = 'price'
    end
    object GetByIdProductnote: TWideStringField
      FieldName = 'note'
      Origin = 'note'
      Size = 8190
    end
    object GetByIdProductsticker: TIntegerField
      FieldName = 'sticker'
      Origin = 'sticker'
    end
  end
  object addCallbackQuery: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'insert into exam.callbacks VALUES (:id, :name, :email, :text);')
    Left = 344
    Top = 48
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 10
      end
      item
        Name = 'NAME'
        DataType = ftString
        ParamType = ptInput
        Value = 'test'
      end
      item
        Name = 'EMAIL'
        DataType = ftString
        ParamType = ptInput
        Value = 'test'
      end
      item
        Name = 'TEXT'
        DataType = ftString
        ParamType = ptInput
        Value = 'test'
      end>
  end
end
