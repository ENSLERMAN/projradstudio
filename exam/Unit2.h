//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Phys.PG.hpp>
#include <FireDAC.Phys.PGDef.hpp>
//---------------------------------------------------------------------------
class TDataModule2 : public TDataModule
{
__published:	// IDE-managed Components
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysPgDriverLink *FDPhysPgDriverLink1;
	TFDConnection *FDConnection1;
	TFDQuery *queryProducts;
	TIntegerField *queryProductsid;
	TIntegerField *queryProductscategory_id;
	TWideStringField *queryProductsname;
	TIntegerField *queryProductsgram;
	TIntegerField *queryProductskcal;
	TFloatField *queryProductsprice;
	TWideStringField *queryProductsnote;
	TIntegerField *queryProductssticker;
	TFDQuery *GetByIdProduct;
	TIntegerField *GetByIdProductid;
	TIntegerField *GetByIdProductcategory_id;
	TWideStringField *GetByIdProductname;
	TIntegerField *GetByIdProductgram;
	TIntegerField *GetByIdProductkcal;
	TFloatField *GetByIdProductprice;
	TWideStringField *GetByIdProductnote;
	TIntegerField *GetByIdProductsticker;
	TFDQuery *addCallbackQuery;
private:	// User declarations
public:		// User declarations
	__fastcall TDataModule2(TComponent* Owner);
	void AddCallback(int id, String name, String email, String text);
};
//---------------------------------------------------------------------------
extern PACKAGE TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
#endif
