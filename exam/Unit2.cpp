//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
__fastcall TDataModule2::TDataModule2(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void TDataModule2::AddCallback(int id, String name, String email, String text) {
	addCallbackQuery->ParamByName("id")->Value = id;
	addCallbackQuery->ParamByName("name")->Value = name;
	addCallbackQuery->ParamByName("email")->Value = email;
	addCallbackQuery->ParamByName("text")->Value = text;
	addCallbackQuery->Execute();
}
