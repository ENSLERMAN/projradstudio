//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include "Unit2.h"
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TTabControl *tabs1;
	TTabItem *tabMenu;
	TTabItem *tabList;
	TGridPanelLayout *GridPanelLayout1;
	TButton *btnList;
	TButton *btnSale;
	TButton *Button4;
	TButton *Button5;
	TButton *btnBack;
	TButton *Button7;
	TButton *Button2;
	TBindSourceDB *BindSourceDB1;
	TTabControl *tabs2;
	TTabItem *products;
	TTabItem *product;
	TListView *ListView1;
	TBindingsList *BindingsList1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label9;
	TLabel *Label10;
	TLabel *Label11;
	TLabel *Label12;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TLinkPropertyToField *LinkPropertyToFieldText6;
	TTabItem *tabDelivery;
	TLabel *Label13;
	TLabel *Label14;
	TTabItem *tabContacts;
	TLabel *��������;
	TLabel *Label15;
	TLabel *Label16;
	TStyleBook *StyleBook1;
	TStyleBook *StyleBook2;
	TButton *Button3;
	TTabItem *tabCart;
	TButton *btnAddToCart;
	TTabItem *tabCallback;
	TLabel *Label17;
	TLabel *Label18;
	TLabel *Label19;
	TLabel *Label20;
	TEdit *edName;
	TEdit *edEmail;
	TMemo *edText;
	TButton *Button6;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall btnListClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall btnAddToCartClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall btnBackClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
	int id = 1;
	void AddToCart(int id);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
