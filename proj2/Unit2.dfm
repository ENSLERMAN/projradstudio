object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 517
  Width = 704
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 496
    Top = 448
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=ensler'
      'User_Name=ensler'
      'Password=b4uld103'
      'Server=ensler.ru'
      'MetaDefSchema=radshit'
      'DriverID=PG')
    Connected = True
    LoginPrompt = False
    Left = 128
    Top = 88
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    VendorLib = 'C:\code\projRadStudio\proj2\VendorLibs\libpq.dll'
    Left = 592
    Top = 448
  end
  object FDQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select *'
      
        'from radshit.users inner join radshit.posts on radshit.users.pos' +
        't=radshit.posts.id;')
    Left = 336
    Top = 240
    object FDQuery1id: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQuery1name: TWideStringField
      FieldName = 'name'
      Origin = 'name'
      Size = 8190
    end
    object FDQuery1surname: TWideStringField
      FieldName = 'surname'
      Origin = 'surname'
      Size = 8190
    end
    object FDQuery1patronymic: TWideStringField
      FieldName = 'patronymic'
      Origin = 'patronymic'
      Size = 8190
    end
    object FDQuery1post: TIntegerField
      FieldName = 'post'
      Origin = 'post'
    end
    object FDQuery1id_1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_1'
      Origin = 'id'
    end
    object FDQuery1postname: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'postname'
      Origin = 'postname'
      Size = 8190
    end
  end
  object getById: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select *'
      
        'from radshit.users inner join radshit.posts on radshit.users.pos' +
        't = radshit.posts.id'
      'where radshit.users.id =:ID;')
    Left = 416
    Top = 200
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 2
      end>
    object getByIdid: TIntegerField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object getByIdname: TWideStringField
      FieldName = 'name'
      Origin = 'name'
      Size = 8190
    end
    object getByIdsurname: TWideStringField
      FieldName = 'surname'
      Origin = 'surname'
      Size = 8190
    end
    object getByIdpatronymic: TWideStringField
      FieldName = 'patronymic'
      Origin = 'patronymic'
      Size = 8190
    end
    object getByIdpost: TIntegerField
      FieldName = 'post'
      Origin = 'post'
    end
    object getByIdid_1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_1'
      Origin = 'id'
    end
    object getByIdpostname: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'postname'
      Origin = 'postname'
      Size = 8190
    end
  end
  object addUser: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'insert into radshit.users VALUES (:ID,:name, :surname, :pat, :po' +
        'st)')
    Left = 504
    Top = 272
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 11
      end
      item
        Name = 'name'
        DataType = ftString
        ParamType = ptInput
        Value = 'asdasdasd'
      end
      item
        Name = 'surname'
        DataType = ftString
        ParamType = ptInput
        Value = 'asdasddasdad'
      end
      item
        Name = 'pat'
        DataType = ftString
        ParamType = ptInput
        Value = 'asdasddasd'
      end
      item
        Name = 'post'
        DataType = ftInteger
        ParamType = ptInput
        Value = '2'
      end>
  end
end
