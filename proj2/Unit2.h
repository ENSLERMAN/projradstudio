//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Phys.PG.hpp>
#include <FireDAC.Phys.PGDef.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Param.hpp>
//---------------------------------------------------------------------------
class TDataModule2 : public TDataModule
{
__published:	// IDE-managed Components
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDConnection *FDConnection1;
	TFDPhysPgDriverLink *FDPhysPgDriverLink1;
	TFDQuery *FDQuery1;
	TIntegerField *FDQuery1id;
	TWideStringField *FDQuery1name;
	TWideStringField *FDQuery1surname;
	TWideStringField *FDQuery1patronymic;
	TIntegerField *FDQuery1post;
	TIntegerField *FDQuery1id_1;
	TWideStringField *FDQuery1postname;
	TFDQuery *getById;
	TIntegerField *getByIdid;
	TWideStringField *getByIdname;
	TWideStringField *getByIdsurname;
	TWideStringField *getByIdpatronymic;
	TIntegerField *getByIdpost;
	TIntegerField *getByIdid_1;
	TWideStringField *getByIdpostname;
	TFDQuery *addUser;
private:	// User declarations
public:		// User declarations
	__fastcall TDataModule2(TComponent* Owner);

	void LoadById(int id);
	void AddNewUser(int id, String name, String surname, String pat, int post);
};
//---------------------------------------------------------------------------
extern PACKAGE TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
#endif
