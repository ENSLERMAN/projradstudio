//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Project1PCH1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnBackClick(TObject *Sender)
{
    tc->ActiveTab = tabUsers;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	DataModule2->LoadById(StrToInt(AItem->Text));
	tc->GotoVisibleTab(tabProfile->Index);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	id++;
	tc->ActiveTab = tabUsers;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnEditClick(TObject *Sender)
{
	tc->GotoVisibleTab(tabEditUser->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAddClick(TObject *Sender)
{
	tc->ActiveTab = tabAddUser;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnSendNewUserClick(TObject *Sender)
{
	id++;
	switch (comboPosts->ItemIndex) {
		case 1:
			idPost = 1;
			break;
		case 2:
			idPost = 2;
			break;
		case 3:
			idPost = 3;
			break;
		case 4:
			idPost = 4;
			break;
		case 5:
			idPost = 5;
			break;
		case 6:
			idPost = 6;
			break;
		case 7:
			idPost = 7;
			break;
		case 8:
			idPost = 8;
			break;
		case 9:
			idPost = 9;
			break;
    }
	DataModule2->AddNewUser(id, editName->Text, editSurname->Text, editPat->Text, idPost);
}
//---------------------------------------------------------------------------

