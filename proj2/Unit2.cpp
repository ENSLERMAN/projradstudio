//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TDataModule2 *DataModule2;
//---------------------------------------------------------------------------
__fastcall TDataModule2::TDataModule2(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

void TDataModule2::LoadById(int id) {
	getById->Close();
	getById->ParamByName("ID")->Value = id;
	getById->Open();
}

void TDataModule2::AddNewUser(int id, String name, String surname, String pat, int post) {
	addUser->ParamByName("id")->Value = id;
	addUser->ParamByName("name")->Value = name;
	addUser->ParamByName("surname")->Value = surname;
	addUser->ParamByName("pat")->Value = pat;
	addUser->ParamByName("post")->Value = post;
	addUser->Execute();
}
