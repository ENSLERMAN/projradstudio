//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include "Unit2.h"
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tabUsers;
	TTabItem *tabAbout;
	TListView *ListView1;
	TToolBar *ToolBar1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TTabItem *tabAddUser;
	TButton *btnBack;
	TButton *btnAdd;
	TTabItem *tabEditUser;
	TTabItem *tabProfile;
	TLinkFillControlToField *LinkFillControlToField1;
	TLabel *Label1;
	TLabel *labelName;
	TLabel *label2;
	TLabel *labelSurname;
	TLabel *Label3;
	TLabel *labelPat;
	TLabel *Label4;
	TLabel *labelPost;
	TBindSourceDB *BindSourceDB2;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TButton *btnEdit;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLabel *Label5;
	TLabel *Label6;
	TEdit *editName;
	TEdit *editPat;
	TEdit *editSurname;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label9;
	TButton *btnSendNewUser;
	TComboBox *comboPosts;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TListBoxItem *ListBoxItem6;
	TListBoxItem *ListBoxItem7;
	TListBoxItem *ListBoxItem8;
	TListBoxItem *ListBoxItem9;
	TBindSourceDB *BindSourceDB3;
	void __fastcall btnBackClick(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall btnEditClick(TObject *Sender);
	void __fastcall btnAddClick(TObject *Sender);
	void __fastcall btnSendNewUserClick(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
	int id = 20;
    int idPost;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
